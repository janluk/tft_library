/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "usart.h"
#include "spi.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "string.h"
#include "stdio.h"

#include "tft_ili9341.h"
#include "gfx_color.h"
#include "fonts/font_8x5.h"
#include "logo_color.h"

#include "fonts_plus/arialBlack_20pt.h"
#include "gfx_font_plus.h"

#include "touch_XPT2046.h"

#include "paint.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define COLOR_DELAY 1000
#define PB_SIZE 30
#define PB_SPACE 5
#define PB1x (PB_SIZE * 1)
#define PB2x ((PB_SIZE * 2) + PB_SPACE)
#define PB3x ((PB_SIZE * 3) + PB_SPACE)
#define PB4x ((PB_SIZE * 4) + PB_SPACE)
#define PB5x ((PB_SIZE * 5) + PB_SPACE)
#define PB6x ((PB_SIZE * 6) + PB_SPACE)
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
ili9342_t tft1;
xpt2046_t touch1;
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
char msg[32];

uint16_t xTouchRead;
uint16_t yTouchRead;

uint16_t PB_color = ILI9341_BLACK;
uint8_t PB_sizePixel;
uint8_t drawMenu = 1;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_LPUART1_UART_Init();
  MX_SPI2_Init();
  MX_SPI3_Init();

  /* Initialize interrupts */
  MX_NVIC_Init();
  /* USER CODE BEGIN 2 */
  ili9345_init(&tft1, &hspi2);
  GFX_SetFont(font_8x5);
  GFX_SetFontSize(2);

  FP_setFont(&arialBlack_20ptFontInfo);

  ili9341_clearPixel(&tft1, ILI9341_WHITE);
  GFX_Image(&tft1, 0, 0, logo_color, 240, 72);

//  FP_putString(&tft1, (uint8_t*)"Twój główny ś", 0, 85, ILI9341_BLACK, BG_COLOR, ILI9341_RED);

  xpt2046_init(&touch1, &hspi3, EXTI0_IRQn);
//  xpt2046_calibrationTouch(&touch1, &tft1);
  ili9341_clearPixel(&tft1, ILI9341_WHITE);
//  GFX_DrawString(&tft1, 50, 100, "Hareo", ILI9341_BLUE);

//  for (uint16_t i = 0; i < ILI9341_TFTWIDTH; i++)
//  {
//	  for (uint16_t j = 0; j < ILI9341_TFTHEIGHT; j++)
//	  {
//		  ili9341_writePixel(&tft1, i, j, ILI9341_WHITE);
//	  }
//  }

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  xpt2046_task(&touch1);

	  Paint(&tft1, &touch1);

//	ili9341_clearPixel(&tft1, ILI9341_WHITE);
//	GFX_DrawRectangle(&tft1, 0, 0, 50, 100, ILI9341_BLACK);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_BLACK);
//	GFX_DrawRectangle(&tft1, 0, 0, 50, 100, ILI9341_NAVY);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_NAVY);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_DARKGREEN);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_DARKCYAN);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_MAROON);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_PURPLE);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_OLIVE);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_LIGHTGREY);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_DARKGREY);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_BLUE);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_GREEN);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_CYAN);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_RED);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_MAGENTA);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_YELLOW);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_WHITE);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_ORANGE);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_GREENYELLOW);
//	HAL_Delay(COLOR_DELAY);
//	ili9341_clearPixel(&tft1, ILI9341_PINK);
//	HAL_Delay(COLOR_DELAY);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  HAL_PWREx_ControlVoltageScaling(PWR_REGULATOR_VOLTAGE_SCALE1_BOOST);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = RCC_PLLM_DIV4;
  RCC_OscInitStruct.PLL.PLLN = 85;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = RCC_PLLQ_DIV2;
  RCC_OscInitStruct.PLL.PLLR = RCC_PLLR_DIV2;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_4) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief NVIC Configuration.
  * @retval None
  */
static void MX_NVIC_Init(void)
{
  /* EXTI0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(EXTI0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(EXTI0_IRQn);
}

/* USER CODE BEGIN 4 */
void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
{
	if(GPIO_Pin == TOUCH_IRQ_Pin)
	{
		xpt2046_IRQ(&touch1);
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
