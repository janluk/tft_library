/*
 * tft_ili9341.c
 *
 *  Created on: Nov 11, 2023
 *      Author: Jan Lukaszewicz
 */

#include "tft_ili9341.h"
#include "main.h" // in main are GPIO definitions

static void ili9345_delay(uint32_t ms)
{
	HAL_Delay(ms);
}

static void ili9345_hwReset(ili9342_t *tft)
{
	HAL_GPIO_WritePin(TFT_RST_GPIO_Port, TFT_RST_Pin, GPIO_PIN_RESET);
	ili9345_delay(10);
	HAL_GPIO_WritePin(TFT_RST_GPIO_Port, TFT_RST_Pin, GPIO_PIN_SET);
	ili9345_delay(10);
}

static tft_status_t ili9345_sendToTFT(ili9342_t *tft, uint8_t *buf, uint32_t bufLenght)
{
#if (ILI9341_OPTIMIZE_HAL_SP1 == 1)

	uint32_t timeOutTickStart = HAL_GetTick();

	// 1.29s
	while (bufLenght > 0U)
	{
		/* Wait until TXE flag is set to send data */
		if (__HAL_SPI_GET_FLAG(tft->hspi, SPI_FLAG_TXE))
		{
			if (bufLenght > 1U)
			{
				/* write on the data register in packing mode */
				tft->hspi->Instance->DR = *((uint16_t*) buf);
				buf += sizeof(uint16_t);
				bufLenght -= 2U;
			} else
			{
				*((__IO uint8_t*) &tft->hspi->Instance->DR) = (*buf);
				buf++;
				bufLenght--;

			}
		} else
		{
			/* Timeout management */
			if ((((HAL_GetTick() - timeOutTickStart) >= ILI9345_TRANSMIT_TIMEOUT) && (ILI9345_TRANSMIT_TIMEOUT != HAL_MAX_DELAY)) || (ILI9345_TRANSMIT_TIMEOUT == 0U))
			{
				return tft_HAL_TIMEOUT;
			}
		}
	}
	// Wait for Transfer end
	while (__HAL_SPI_GET_FLAG(tft->hspi, SPI_FLAG_BSY) != RESET)
	{

	}

#else

	if(0 != HAL_SPI_Transmit(tft->hspi, buf, bufLenght, ILI9345_TRANSMIT_TIMEOUT))
	{
		return tft_HAL_TIMEOUT;
	}

#endif

	return tft_NoError;
}

static void ili9345_sendCommand(ili9342_t *tft, uint8_t command, uint32_t lenght)
{

#if (ILI9341_USE_CS == 1)
	// CS Low
	HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_RESET);
#endif

	// DC low - command
	HAL_GPIO_WritePin(TFT_DC_GPIO_Port, TFT_DC_Pin, GPIO_PIN_RESET);

	// send command
	ili9345_sendToTFT(tft, &command, lenght);

#if (ILI9341_USE_CS == 1)
	// CS High
	HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_SET);
#endif
}


static void ili9345_sendCommandAndData(ili9342_t *tft, uint8_t command, uint8_t *data, uint32_t lenght)
{
#if (ILI9341_USE_CS == 1)
	// CS Low
	HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_RESET);
#endif

	// DC low - command
	HAL_GPIO_WritePin(TFT_DC_GPIO_Port, TFT_DC_Pin, GPIO_PIN_RESET);
	// send command
	ili9345_sendToTFT(tft, &command, 1);

	// DC high - data
	HAL_GPIO_WritePin(TFT_DC_GPIO_Port, TFT_DC_Pin, GPIO_PIN_SET);
	// send data
	ili9345_sendToTFT(tft, data, lenght);

#if (ILI9341_USE_CS == 1)
		// CS High
	HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_SET);
#endif
}

//
// TFT functions
//
void ili9341_setAddrWindow(ili9342_t *tft, uint16_t x1, uint16_t y1, uint16_t width, uint16_t height)
{
	uint16_t x2 = (x1 + width - 1), y2 = (y1 + height - 1);
	uint8_t data[4];

	// Column address set
	data[0] = (x1 >> 8);	//MSB
	data[1] = (x1 & 0xFF);	//LSB
	data[2] = (x2 >> 8);
	data[3] = (x2 & 0xFF);
	ili9345_sendCommandAndData(tft,ILI9341_CASET, data, 4);

	// Row address set
    data[0] = (y1 >> 8);
    data[1] = (y1 & 0xFF);
    data[2] = (y2 >> 8);
    data[3] = (y2 & 0xFF);
    ili9345_sendCommandAndData(tft,ILI9341_PASET, data, 4);
}

tft_status_t ili9341_writePixel(ili9342_t *tft, uint16_t x, uint16_t y, uint16_t color)
{
	uint8_t data[2];

	if ((x > ILI9341_TFTWIDTH) || (y > ILI9341_TFTHEIGHT))
	{
		return tft_ErrorIncorrectPosition;
	}

	// Set Window for 1x1 pixel
	ili9341_setAddrWindow(tft, x, y, 1, 1);

	// Fulfill buffer with color
    data[0] = (color >> 8);
    data[1] = (color & 0xFF);
    // Write color to TFT RAM
    ili9345_sendCommandAndData(tft, ILI9341_RAMWR, data, 2);

	return tft_NoError;
}

void ili9341_clearAreaPixel(ili9342_t *tft, uint16_t xStart, uint16_t yStart, uint16_t width, uint16_t height, uint16_t color)
{
	// spi speed: 10kHz whole TFT has drawn in 135 ms
	// spi speed: 20kHz 						58 ms

	uint32_t bufLenght = width * height;
	// set window for whole screen
	ili9341_setAddrWindow(tft, xStart, yStart, width, height);
	// set RAM writing
	ili9345_sendCommand(tft, ILI9341_RAMWR, 1);

#if (ILI9341_USE_CS == 1)
		// CS Low
	HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_RESET);
#endif

	// DC hight - data
	HAL_GPIO_WritePin(TFT_DC_GPIO_Port, TFT_DC_Pin, GPIO_PIN_SET);

	while (bufLenght > 0U)
	{
		/* Wait until TXE flag is set to send data */
	    if (__HAL_SPI_GET_FLAG(tft->hspi, SPI_FLAG_TXE))
	    {
	        /* write on the data register in packing mode */
	      	tft->hspi->Instance->DR = color;
        	bufLenght --;

	    }
	}

	while(__HAL_SPI_GET_FLAG(tft->hspi, SPI_FLAG_BSY) != RESET)
	{

	}

#if (ILI9341_USE_CS == 1)
		// CS High
	HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_SET);
#endif
}

void ili9341_clearPixel(ili9342_t *tft, uint16_t color)
{
	// spi speed: 10kHz whole TFT has drawn in 135 ms
	// spi speed: 20kHz 						58 ms
	uint32_t bufLenght = ILI9341_TFTWIDTH * ILI9341_TFTHEIGHT;
	// set window for whole screen
	ili9341_setAddrWindow(tft, 0, 0, ILI9341_TFTWIDTH, ILI9341_TFTHEIGHT);
	// set RAM writing
	ili9345_sendCommand(tft, ILI9341_RAMWR, 1);

#if (ILI9341_USE_CS == 1)
		// CS Low
	HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_RESET);
#endif

	// DC hight - data
	HAL_GPIO_WritePin(TFT_DC_GPIO_Port, TFT_DC_Pin, GPIO_PIN_SET);

	while (bufLenght > 0U)
	{
		/* Wait until TXE flag is set to send data */
	    if (__HAL_SPI_GET_FLAG(tft->hspi, SPI_FLAG_TXE))
	    {
	        /* write on the data register in packing mode */
	      	tft->hspi->Instance->DR = color;
        	bufLenght --;

	    }
	}

	while(__HAL_SPI_GET_FLAG(tft->hspi, SPI_FLAG_BSY) != RESET)
	{

	}

#if (ILI9341_USE_CS == 1)
		// CS High
	HAL_GPIO_WritePin(SPI2_CS_GPIO_Port, SPI2_CS_Pin, GPIO_PIN_SET);
#endif
}

tft_status_t ili9341_setRotation(ili9342_t *tft, uint8_t rotation)
{
  if(rotation > 4)
  {
	  return tft_ErrorIncorrectRotation;
  }

  switch (rotation) {
  case 0:
	  rotation = (ILI9341_MADCTL_MX | ILI9341_MADCTL_BGR);
    break;
  case 1:
	  rotation = (ILI9341_MADCTL_MV | ILI9341_MADCTL_BGR);
    break;
  case 2:
	  rotation = (ILI9341_MADCTL_MY | ILI9341_MADCTL_BGR);
    break;
  case 3:
	  rotation = (ILI9341_MADCTL_MX | ILI9341_MADCTL_MY | ILI9341_MADCTL_MV | ILI9341_MADCTL_BGR);
    break;
  }

  ili9345_sendCommandAndData(tft, ILI9341_MADCTL, &rotation, 1);

  return tft_NoError;
}

static const uint8_t initcmd[] = {
  0xEF, 3, 0x03, 0x80, 0x02,
  0xCF, 3, 0x00, 0xC1, 0x30,
  0xED, 4, 0x64, 0x03, 0x12, 0x81,
  0xE8, 3, 0x85, 0x00, 0x78,
  0xCB, 5, 0x39, 0x2C, 0x00, 0x34, 0x02,
  0xF7, 1, 0x20,
  0xEA, 2, 0x00, 0x00,
  ILI9341_PWCTR1  , 1, 0x23,             // Power control VRH[5:0]
  ILI9341_PWCTR2  , 1, 0x10,             // Power control SAP[2:0];BT[3:0]
  ILI9341_VMCTR1  , 2, 0x3e, 0x28,       // VCM control
  ILI9341_VMCTR2  , 1, 0x86,             // VCM control2
  ILI9341_MADCTL  , 1, 0x48,             // Memory Access Control 0x48
  ILI9341_VSCRSADD, 1, 0x00,             // Vertical scroll zero
  ILI9341_PIXFMT  , 1, 0x55,
  ILI9341_FRMCTR1 , 2, 0x00, 0x18,
  ILI9341_DFUNCTR , 3, 0x08, 0x82, 0x27, // Display Function Control
  0xF2, 1, 0x00,                         // 3Gamma Function Disable
  ILI9341_GAMMASET , 1, 0x01,             // Gamma curve selected
  ILI9341_GMCTRP1 , 15, 0x0F, 0x31, 0x2B, 0x0C, 0x0E, 0x08, // Set Gamma
    0x4E, 0xF1, 0x37, 0x07, 0x10, 0x03, 0x0E, 0x09, 0x00,
  ILI9341_GMCTRN1 , 15, 0x00, 0x0E, 0x14, 0x03, 0x11, 0x07, // Set Gamma
    0x31, 0xC1, 0x48, 0x08, 0x0F, 0x0C, 0x31, 0x36, 0x0F,
  ILI9341_SLPOUT  , 0x80,                // Exit Sleep
  ILI9341_DISPON  , 0x80,                // Display on
  0x00                                   // End of list
};

void ili9345_init(ili9342_t *tft, SPI_HandleTypeDef *hspi)
{
	tft->hspi = hspi;

	uint8_t cmd, x, numArgs;
	const uint8_t *addr = initcmd;

#if(ILI9341_OPTIMIZE_HAL_SP1 == 1)
	__HAL_SPI_ENABLE(tft->hspi);
#endif

	#if (ILI9341_USE_HW_RESET == 1 )
		ili9345_hwReset(tft);
	#else
		ili9345_sendCommandOneByte(tft, ILI9341_SWRESET); // Engage software reset
		ili9345_delay(150);
	#endif

	while ((cmd = *(addr++)) > 0)
	{
		x = *(addr++);
		numArgs = x & 0x7F;

		ili9345_sendCommandAndData(tft, cmd, (uint8_t*)addr, numArgs);
		addr += numArgs;

		if (x & 0x80)
		{
			ili9345_delay(150);

		}
	}
	ili9341_setRotation(tft, ILI9341_ROTATION);
}
