/*
 * touch_XPT2064.c
 *
 *  Created on: Nov 30, 2023
 *      Author: Jan Lukaszewicz
 */

#include "touch_XPT2046.h"
#include "main.h"
#include "gfx_color.h"

//
// Calibration data used for each coords calculation
//
#if (TOUCH_ROTATION == 0)
calibData_t calibData = {-.0009337, -.0636839, 250.342, -.0889775, -.00118110, 356.538}; // default calibration data
#endif
#if (TOUCH_ROTATION == 1)
calibData_t calibData = {-.0885542, .0016532, 349.800, .0007309, .06543699, -15.290}; // default calibration data
#endif
#if (TOUCH_ROTATION == 2)
calibData_t calibData = {.0006100, .0647828, -13.634, .0890609, .0001381, -35.73}; // default calibration data
#endif
#if (TOUCH_ROTATION == 3)
calibData_t calibData = {.0902216, .0006510, -38.657, -.0010005, -.0667030, 258.08}; // default calibration data
#endif

//
//	Calibration data - pattern points
//
#if (TOUCH_ROTATION == 0 || TOUCH_ROTATION == 2)
static const uint16_t calA[] = {10, 10};	// Calibration points
static const uint16_t calB[] = {80, 280};
static const uint16_t calC[] = {200, 170};
#endif
#if (TOUCH_ROTATION == 1 || TOUCH_ROTATION == 3)
static const uint16_t calA[] = {20, 25};	// Calibration points
static const uint16_t calB[] = {160, 220};
static const uint16_t calC[] = {300, 110};
#endif

//
// Calibration points - read from panel todo wywalic do struktury
//
uint16_t calA_raw[] = {0, 0}; // Read data
uint16_t calB_raw[] = {0, 0};
uint16_t calC_raw[] = {0, 0};


// HW FUNCTIONS
static void xpt2046_getRowData(xpt2046_t *touch)
{
	#if (XTP2046_USE_CS == 1)
	HAL_GPIO_WritePin(TOUCH_CS_GPIO_Port, TOUCH_CS_Pin, GPIO_PIN_RESET);
	#endif

	HAL_SPI_TransmitReceive(touch->hspi, touch->sendBuffer, touch->receiveBuffer, 5, XTP2046_SPI_TIMEOUT);

	#if (XTP2046_USE_CS == 1)
	HAL_GPIO_WritePin(TOUCH_CS_GPIO_Port, TOUCH_CS_Pin, GPIO_PIN_SET);
	#endif
}

static void xpt2046_readRawData(xpt2046_t *touch, uint16_t *x, uint16_t *y)
{
	*x = (uint16_t)((touch->receiveBuffer[1] << 8) | (touch->receiveBuffer[2]));
	*y = (uint16_t)((touch->receiveBuffer[3] << 8) | (touch->receiveBuffer[4]));
}

//
// Read Raw ADC data to variables and calculate coords in pixels based on Calibration Data
//
static void xpt2046_readTouchPoint(xpt2046_t *touch, uint16_t *x, uint16_t *y)
{
	uint16_t _x, _y;
	xpt2046_readRawData(touch, &_x, &_y);

	if(touch->calibrationMode == 0)
	{
		// Calculate in Normal Mode
		*x = touch->calibrationData->alpha_x * _x + touch->calibrationData->beta_x * _y + touch->calibrationData->delta_x;
		*y = touch->calibrationData->alpha_y * _x + touch->calibrationData->beta_y * _y + touch->calibrationData->delta_y;
	}
	else
	{
		// Get Raw data in Calibration Mode
		*x = _x;
		*y = _y;
	}
}

void xpt2046_getAverageTouchPoint(xpt2046_t *touch, uint16_t *x, uint16_t *y)
{
	uint32_t averageX = 0, averageY = 0;
	uint8_t i;

	for (i = 0; i < XTP2046_MAX_TOUCH_SAMPLES; i++)
	{
		averageX += touch->samplesTouch[0][i];
		averageY += touch->samplesTouch[1][i];

	}
	*x = averageX / XTP2046_MAX_TOUCH_SAMPLES;
	*y = averageY / XTP2046_MAX_TOUCH_SAMPLES;
}

uint8_t xpt2046_isTouched(xpt2046_t *touch)
{
	if(touch->stateTouch == XPT2046_TOUCHED)
	{
		return 1;
	}
	return 0;
}

void xpt2046_task(xpt2046_t *touch)
{
	switch(touch->stateTouch)
	{
	case XPT2046_IDLE:
		// do notrhing in IDLE - we are waiting for interrupt
		break;

	case XPT2046_PRESAMPLING:
		if((HAL_GetTick() - touch->sampleTimer) > XPT2046_SAMPLE_INTERVAL)
		{
			xpt2046_getRowData(touch);
			xpt2046_readTouchPoint(touch, &touch->samplesTouch[0][touch->sampleCounter], &touch->samplesTouch[1][touch->sampleCounter]);
			touch->sampleCounter++;

			if(touch->sampleCounter == XTP2046_MAX_TOUCH_SAMPLES)
			{
				touch->sampleCounter = 0;
				touch->stateTouch = XPT2046_TOUCHED;
			}

			if(GPIO_PIN_SET == HAL_GPIO_ReadPin(TOUCH_IRQ_GPIO_Port, TOUCH_IRQ_Pin))
			{
				touch->stateTouch = XPT2046_RELEASED;
			}

			touch->sampleTimer = HAL_GetTick();
		}
		break;

	case XPT2046_TOUCHED:

		if((HAL_GetTick() - touch->sampleTimer) > XPT2046_SAMPLE_INTERVAL)
		{
			xpt2046_getRowData(touch);
			xpt2046_readTouchPoint(touch, &touch->samplesTouch[0][touch->sampleCounter], &touch->samplesTouch[1][touch->sampleCounter]);
			touch->sampleCounter++;
			touch->sampleCounter %= XTP2046_MAX_TOUCH_SAMPLES;

			if(GPIO_PIN_SET == HAL_GPIO_ReadPin(TOUCH_IRQ_GPIO_Port, TOUCH_IRQ_Pin))
			{
				// Go to RELEASED state
				touch->stateTouch = XPT2046_RELEASED;
			}

			touch->sampleTimer = HAL_GetTick();
		}
		break;

	case XPT2046_RELEASED:

		touch->stateTouch = XPT2046_IDLE;
		touch->sampleCounter = 0;

		while(HAL_NVIC_GetPendingIRQ(touch->IRQn))
		{
			__HAL_GPIO_EXTI_CLEAR_IT(TOUCH_IRQ_Pin);
			HAL_NVIC_ClearPendingIRQ(touch->IRQn);
		}
		HAL_NVIC_EnableIRQ(touch->IRQn);
		break;

	default:
	    break;
	}
}

//
// Interrupt routine - put in IRQ handler callback
//
void xpt2046_IRQ(xpt2046_t *touch)
{
	// Disable IRQ for avoid false IRQs (Datasheet)
	HAL_NVIC_DisableIRQ(touch->IRQn);
	// Jump to PRESAMPLING state
	touch->stateTouch = XPT2046_PRESAMPLING;
}


void xpt2046_init(xpt2046_t *touch, SPI_HandleTypeDef *hspi, IRQn_Type touchIRQn)
{
	touch->hspi = hspi;
	touch->IRQn = touchIRQn;

	touch->calibrationMode = 0;
	touch->calibrationData = &calibData;

	touch->stateTouch = XPT2046_IDLE;
	touch->sampleCounter = 0;

	touch->sampleTimer = 0;

#if (XTP2046_USE_CS == 1)
	HAL_GPIO_WritePin(TOUCH_CS_GPIO_Port, TOUCH_CS_Pin, GPIO_PIN_SET);
#endif

	//
	// Prepare Send Buffer
	//

	//     (     X    )           (     Y 	 )
	// (000 10010)(000 00000) (000 11010)(000 00000) (00000000)
	//	SendBuffer
	// (    0    )(    1    ) (    2    )(    3    ) (    4   )
		touch->channelSettingsX = 0b10010000;
		touch->channelSettingsY = 0b11010000;

		touch->sendBuffer[0] = 0x80; // Clear settings in IC
		xpt2046_getRowData(touch); // Send clearing command
		HAL_Delay(1); // Wait for clear

		// Fulfill Send Buffer with Channel control bytes
		touch->sendBuffer[0] = (touch->channelSettingsX>>3);
		touch->sendBuffer[1] = (touch->channelSettingsX<<5);
		touch->sendBuffer[2] = (touch->channelSettingsY>>3);
		touch->sendBuffer[3] = (touch->channelSettingsY<<5);
		touch->sendBuffer[4] = 0;

}

//
// Calibration stuff
//

//
// Draw X in circle as calibration point indicator
//
static void xpt2046_drawCalibrationPoint(ili9342_t *tft, uint16_t calX, uint16_t calY)
{
  GFX_DrawCircle(tft, calX, calY, 6, ILI9341_WHITE);
  GFX_DrawLine(tft, calX-4, calY, calX+4, calY, ILI9341_WHITE);
  GFX_DrawLine(tft, calX, calY-4, calX, calY+4, ILI9341_WHITE);
}

//
// Calculate new Calibration data - mathematics with well known 3-point calibration
//
static void xpt2046_calculateCalibrationData(xpt2046_t *touch)
{
	int32_t delta = (calA_raw[0]-calC_raw[0])*(calB_raw[1]-calC_raw[1]) -
	       (calB_raw[0]-calC_raw[0])*(calA_raw[1]-calC_raw[1]);

	touch->calibrationData->alpha_x = (float)((calA[0]-calC[0])*(calB_raw[1]-calC_raw[1]) -
	       (calB[0]-calC[0])*(calA_raw[1]-calC_raw[1])) / delta;

	touch->calibrationData->beta_x = (float)((calA_raw[0]-calC_raw[0])*(calB[0]-calC[0]) -
	       (calB_raw[0]-calC_raw[0])*(calA[0]-calC[0])) / delta;

	touch->calibrationData->delta_x = ((float)calA[0]*(calB_raw[0]*calC_raw[1]-calC_raw[0]*calB_raw[1]) -
	       (float)calB[0]*(calA_raw[0]*calC_raw[1]-calC_raw[0]*calA_raw[1]) +
	       (float)calC[0]*(calA_raw[0]*calB_raw[1]-calB_raw[0]*calA_raw[1])) / delta;

	touch->calibrationData->alpha_y = (float)((calA[1]-calC[1])*(calB_raw[1]-calC_raw[1]) -
	       (calB[1]-calC[1])*(calA_raw[1]-calC_raw[1])) / delta;

	touch->calibrationData->beta_y = (float)((calA_raw[0]-calC_raw[0])*(calB[1]-calC[1]) -
	       (calB_raw[0]-calC_raw[0])*(calA[1]-calC[1])) / delta;

	touch->calibrationData->delta_y = ((float)calA[1]*(calB_raw[0]*calC_raw[1]-calC_raw[0]*calB_raw[1]) -
		       (float)calB[1]*(calA_raw[0]*calC_raw[1]-calC_raw[0]*calA_raw[1]) +
		       (float)calC[1]*(calA_raw[0]*calB_raw[1]-calB_raw[0]*calA_raw[1])) / delta;
}

void xpt2046_calibrationTouch(xpt2046_t *touch, ili9342_t *tft)
{
	uint8_t calCount = 0;
	ili9341_clearPixel(tft, ILI9341_BLACK);
	touch->calibrationMode = 1;

	while(calCount < 4)
	{
		xpt2046_task(touch);

		switch(calCount)
		{
			case 0:
				xpt2046_drawCalibrationPoint(tft, calA[0], calA[1]);
				if(touch->stateTouch == XPT2046_TOUCHED)
				{
					xpt2046_getAverageTouchPoint(touch, &calA_raw[0], &calA_raw[1]);
				}

				if(touch->stateTouch == XPT2046_RELEASED)
				{
					HAL_Delay(200);
					calCount++;
				}
				break;

			case 1: // 2nd point
				GFX_DrawFillRectangle(tft, calA[0]-6, calA[1]-6, 13, 13, ILI9341_BLACK);

				xpt2046_drawCalibrationPoint(tft, calB[0], calB[1]);
				if(touch->stateTouch == XPT2046_TOUCHED)
				{
					xpt2046_getAverageTouchPoint(touch, &calB_raw[0], &calB_raw[1]);
				}
				if(touch->stateTouch == XPT2046_RELEASED)
				{
					HAL_Delay(200);
					calCount++;
				}
				break;
			case 2: // 3rd point
				GFX_DrawFillRectangle(tft, calB[0]-6, calB[1]-6, 13, 13, ILI9341_BLACK);

				xpt2046_drawCalibrationPoint(tft, calC[0], calC[1]);
				if(touch->stateTouch == XPT2046_TOUCHED)
				{
					xpt2046_getAverageTouchPoint(touch, &calC_raw[0], &calC_raw[1]);
				}
				if(touch->stateTouch == XPT2046_RELEASED)
				{
					HAL_Delay(200);
					calCount++;
				}
				break;
			case 3: // calculate and save calibration data,
				GFX_DrawFillRectangle(tft, calC[0]-6, calC[1]-6, 13, 13, ILI9341_BLACK);

				xpt2046_calculateCalibrationData(touch);
				calCount++;


				break;

			default:
				break;
		}
	}
	touch->calibrationMode = 0;
}


