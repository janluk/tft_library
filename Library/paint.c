/*
 * paint.c
 *
 *  Created on: Dec 16, 2023
 *      Author: Jan Lukaszewicz
 */


#include "paint.h"

#include "gfx_color.h"
#include "gfx_font_plus.h"

#define DELAY_RUBBER_BUTTON 250
#define DELAY_PIXEL_BUTTONS 250
#define MAX_DRAW_PIXEL_SIZE 13

#define DRAW_WINDOW_X_START 0
#define DRAW_WINDOW_X_STOP ILI9341_TFTWIDTH
#define DRAW_WINDOW_Y_START 30
#define DRAW_WINDOW_Y_STOP (ILI9341_TFTHEIGHT-35)

#define CLEAR_BUTTON_X (ILI9341_TFTWIDTH-52)
#define CLEAR_BUTTON_Y 3
#define CLEAR_BUTTON_WIDTH 23
#define CLEAR_BUTTON_HEIGHT 25

#define PIXEL_BUTTON_Y 3
#define PIXEL_BUTTON_WIDTH 25
#define PIXEL_BUTTON_HEIGHT 25
#define PIXEL_PLUS_SIZE_BUTTON_X 	(ILI9341_TFTWIDTH-79)
#define PIXEL_SIZE_POINT_X 			(ILI9341_TFTWIDTH-106)
#define PIXEL_MINUS_SIZE_BUTTON_X 	(ILI9341_TFTWIDTH-106-25)
#define RUBBER_SIZE_BUTTON_X 		(ILI9341_TFTWIDTH-133-25)


#define COLOR_BUTTON_WIDTH 20
#define COLOR_BUTTON_HEIGHT 20

#define USED_COLORS	10

static ili9342_t *tftPaint;
static xpt2046_t *touchPaint;

uint32_t paint_lastTick;

typedef enum
{
	PAINT_INIT, // Build GUI
	PAINT_DRAW, // Read Touch and draw pixels
	PAINT_CLEAR // Clear drawing area
} PaintState;

PaintState State = PAINT_INIT; // Initialization state for Paint State Machine

uint16_t UsedColors[USED_COLORS] = {
				ILI9341_BLACK,
				ILI9341_DARKGREEN,
				ILI9341_MAROON,
				ILI9341_PURPLE,
				ILI9341_OLIVE,
				ILI9341_BLUE,
				ILI9341_GREEN,
				ILI9341_RED,
				ILI9341_YELLOW,
				ILI9341_PINK


}; // Colors table

uint16_t CurrentColor = ILI9341_BLACK; // Default color
uint16_t LastColor;

uint8_t DrawPixelSize = 1;

void DrawSizePixelPoint(void)
{
	GFX_DrawFillCircle(tftPaint, PIXEL_SIZE_POINT_X+13, PIXEL_BUTTON_Y+12, DrawPixelSize, CurrentColor);
}

//
// Draw Color indicator as Circle above the drawing area
//
void ColorIndicator(void)
{
	DrawSizePixelPoint();
	FP_putString(tftPaint, (uint8_t*)"Paint", 5, 2, CurrentColor, BG_TRANSPARENT, ILI9341_GREEN);
}

//
// Draw Clearing Button above the drawing area
//
void ClearButton(void)
{
	GFX_DrawFillRectangle(tftPaint, CLEAR_BUTTON_X, CLEAR_BUTTON_Y, CLEAR_BUTTON_WIDTH, CLEAR_BUTTON_HEIGHT, ILI9341_DARKGREEN); // Button Color

	FP_putString(tftPaint, (uint8_t*)"C", CLEAR_BUTTON_X+3, CLEAR_BUTTON_Y, ILI9341_WHITE, BG_TRANSPARENT, ILI9341_DARKGREEN); // Button text
}


void DrawSizePixelButton(void)
{
	GFX_DrawFillRectangle(tftPaint, PIXEL_PLUS_SIZE_BUTTON_X, PIXEL_BUTTON_Y, PIXEL_BUTTON_WIDTH, PIXEL_BUTTON_HEIGHT, ILI9341_BLUE); // Button Color
	FP_putString(tftPaint, (uint8_t*)"+", PIXEL_PLUS_SIZE_BUTTON_X+5, PIXEL_BUTTON_Y, ILI9341_WHITE, BG_TRANSPARENT, ILI9341_BLUE); // Button text

	GFX_DrawFillRectangle(tftPaint, PIXEL_MINUS_SIZE_BUTTON_X, PIXEL_BUTTON_Y, PIXEL_BUTTON_WIDTH, PIXEL_BUTTON_HEIGHT, ILI9341_BLUE); // Button Color
	FP_putString(tftPaint, (uint8_t*)"-", PIXEL_MINUS_SIZE_BUTTON_X+5, PIXEL_BUTTON_Y, ILI9341_WHITE, BG_TRANSPARENT, ILI9341_BLUE); // Button text

}

void DrawRubberButton(colorType color)
{
	GFX_DrawFillRectangle(tftPaint, RUBBER_SIZE_BUTTON_X, PIXEL_BUTTON_Y, PIXEL_BUTTON_WIDTH, PIXEL_BUTTON_HEIGHT, color); // Button Color
	FP_putString(tftPaint, (uint8_t*)"R", RUBBER_SIZE_BUTTON_X+5, PIXEL_BUTTON_Y, ILI9341_WHITE, BG_TRANSPARENT, ILI9341_RED); // Button text

}

//
// Draw each button for color change in one loop
//
//
//
void ColorButtons(void)
{
	uint8_t i;
	for(i = 1; i <= USED_COLORS; i++) // For each color
	{
		GFX_DrawFillRectangle(tftPaint, (i*(ILI9341_TFTWIDTH/(USED_COLORS+1)))-(COLOR_BUTTON_WIDTH/2), ILI9341_TFTHEIGHT-25, COLOR_BUTTON_WIDTH, COLOR_BUTTON_HEIGHT, UsedColors[i-1]);
	}
}

uint8_t IsColorButtonTouched(uint16_t x, uint16_t y)
{
	uint8_t i;
	for(i = 1; i <= USED_COLORS; i++) // For each color used
	{
		 // Check if Touch point is higher than X begin of current color button
		if(x > (i*(ILI9341_TFTWIDTH/(USED_COLORS+1)))-(COLOR_BUTTON_WIDTH/2))
		{
			 // Check if Touch point is lower than X end of current color button
			if(x < (i*(ILI9341_TFTWIDTH/(USED_COLORS+1)))+(COLOR_BUTTON_WIDTH/2))
			{
				// Check if Touch point is higher than Y begin of current color button
				if(y > ILI9341_TFTHEIGHT-25)
				{
					// Check if Touch point is lower than Y end of current color button
					if(y < (ILI9341_TFTHEIGHT-25+COLOR_BUTTON_HEIGHT))
					{
						// If we are sure that touched point was inside current (i) button - return button number
						return i;
					}
				}
			}
		}
	}
	// If no color button touched
	return 0;
}

uint8_t IsRubberButtonTouched(uint16_t x, uint16_t y)
{
	// Check if Touch point is higher than X begin of clear button
	if(x > RUBBER_SIZE_BUTTON_X)
	{
		// Check if Touch point is higher than X end of clear button
		if(x < (RUBBER_SIZE_BUTTON_X + PIXEL_BUTTON_WIDTH))
		{
			// Check if Touch point is higher than Y begin of clear button
			if(y > PIXEL_BUTTON_Y)
			{
				// Check if Touch point is higher than Y end of clear button
				if(y < (PIXEL_BUTTON_Y+PIXEL_BUTTON_HEIGHT))
				{
					// If we are sure that touched point was inside clear button - return 1
					return 1;
				}
			}
		}
	}
	// If clear button is not touched
	return 0;
}

uint8_t IsMinusButtonTouched(uint16_t x, uint16_t y)
{
	// Check if Touch point is higher than X begin of clear button
	if(x > PIXEL_MINUS_SIZE_BUTTON_X)
	{
		// Check if Touch point is higher than X end of clear button
		if(x < (PIXEL_MINUS_SIZE_BUTTON_X + PIXEL_BUTTON_WIDTH))
		{
			// Check if Touch point is higher than Y begin of clear button
			if(y > PIXEL_BUTTON_Y)
			{
				// Check if Touch point is higher than Y end of clear button
				if(y < (PIXEL_BUTTON_Y+PIXEL_BUTTON_HEIGHT))
				{
					// If we are sure that touched point was inside clear button - return 1
					return 1;
				}
			}
		}
	}
	// If clear button is not touched
	return 0;
}

uint8_t IsPlusButtonTouched(uint16_t x, uint16_t y)
{
	// Check if Touch point is higher than X begin of clear button
	if(x > PIXEL_PLUS_SIZE_BUTTON_X)
	{
		// Check if Touch point is higher than X end of clear button
		if(x < (PIXEL_PLUS_SIZE_BUTTON_X + PIXEL_BUTTON_WIDTH))
		{
			// Check if Touch point is higher than Y begin of clear button
			if(y > PIXEL_BUTTON_Y)
			{
				// Check if Touch point is higher than Y end of clear button
				if(y < (PIXEL_BUTTON_Y+PIXEL_BUTTON_HEIGHT))
				{
					// If we are sure that touched point was inside clear button - return 1
					return 1;
				}
			}
		}
	}
	// If clear button is not touched
	return 0;
}

uint8_t IsClearButtonTouched(uint16_t x, uint16_t y)
{
	// Check if Touch point is higher than X begin of clear button
	if(x > CLEAR_BUTTON_X)
	{
		// Check if Touch point is higher than X end of clear button
		if(x < (CLEAR_BUTTON_X + CLEAR_BUTTON_WIDTH))
		{
			// Check if Touch point is higher than Y begin of clear button
			if(y > CLEAR_BUTTON_Y)
			{
				// Check if Touch point is higher than Y end of clear button
				if(y < (CLEAR_BUTTON_Y+CLEAR_BUTTON_HEIGHT))
				{
					// If we are sure that touched point was inside clear button - return 1
					return 1;
				}
			}
		}
	}
	// If clear button is not touched
	return 0;
}

//
// PAINT_INIT state function
//
void InitScreen(ili9342_t *tft, xpt2046_t *touch)
{
	tftPaint = tft;
	touchPaint = touch;

	// Clear whole display
	ili9341_clearPixel(tftPaint, ILI9341_WHITE);
	// Title
	FP_putString(tftPaint, (uint8_t*)"Paint", 5, 2, ILI9341_BLACK, BG_TRANSPARENT, ILI9341_GREEN);
	// Drawing area
	GFX_DrawRectangle(tftPaint, DRAW_WINDOW_X_START, DRAW_WINDOW_Y_START, DRAW_WINDOW_X_STOP, DRAW_WINDOW_Y_STOP, ILI9341_BLACK);

	DrawRubberButton(ILI9341_RED);
	// change size draw pixel
	DrawSizePixelButton();

	DrawSizePixelPoint();

	// Clear button
	ClearButton();
	// Color buttons
	ColorButtons();

	State = PAINT_DRAW;
}

//
// PAINT_DRAW state function
//
void DrawScreen(void)
{
	// Check if screen was touched
	if(xpt2046_isTouched(touchPaint))
	{
		uint16_t x, y; // Touch points
		uint8_t ColorButtonNubmer; // Color number touched for checking that

		xpt2046_getAverageTouchPoint(touchPaint, &x, &y); // Get the current couched point

		// Check if that point is inside the drawing area
		if((x > DRAW_WINDOW_X_START)&&(x < DRAW_WINDOW_X_STOP)&&(y > DRAW_WINDOW_Y_START + DrawPixelSize)&&(y < DRAW_WINDOW_Y_STOP - DrawPixelSize))
		{
			// If yes - just draw a pixel there with current selected color
//			GFX_DrawPixel(tftPaint, x, y, CurrentColor);
			GFX_DrawFillCircle(tftPaint, x, y, DrawPixelSize, CurrentColor);

		}

		// Check if any Color change button was touched
		ColorButtonNubmer = IsColorButtonTouched(x, y);
		if(ColorButtonNubmer != 0) // If yes
		{
			// Chenge current color
			CurrentColor = UsedColors[ColorButtonNubmer-1];
			// Redrwa current color indicator
			ColorIndicator();
		}

		if(IsRubberButtonTouched(x,y))
		{

			if((HAL_GetTick() - paint_lastTick) >= DELAY_RUBBER_BUTTON)
			{
				if(CurrentColor != ILI9341_WHITE)
				{
					LastColor = CurrentColor;
					CurrentColor = ILI9341_WHITE;
					DrawRubberButton(ILI9341_BLACK);
				}
				else
				{
					CurrentColor = LastColor;
					DrawRubberButton(ILI9341_RED);
				}

				paint_lastTick = HAL_GetTick();
			}
		}

		if(IsMinusButtonTouched(x,y))
		{
			if((HAL_GetTick() - paint_lastTick) >= DELAY_PIXEL_BUTTONS)
			{
				if(DrawPixelSize > 0)
				{
					DrawPixelSize--;

					ili9341_clearAreaPixel(tftPaint, PIXEL_SIZE_POINT_X, 0, 28, 30, ILI9341_WHITE);
//					GFX_DrawFillCircle(tftPaint, PIXEL_SIZE_POINT_X+13, PIXEL_BUTTON_Y+12, MAX_DRAW_PIXEL_SIZE, ILI9341_WHITE);
					DrawSizePixelPoint();
				}

				paint_lastTick = HAL_GetTick();
			}
		}

		if(IsPlusButtonTouched(x,y))
		{
			if((HAL_GetTick() - paint_lastTick) >= DELAY_PIXEL_BUTTONS)
			{
				if(DrawPixelSize < MAX_DRAW_PIXEL_SIZE)
				{
					DrawPixelSize++;
					DrawSizePixelPoint();
				}
				paint_lastTick = HAL_GetTick();
			}

		}

		// Check if any Clearing button was touched
		if(IsClearButtonTouched(x, y))
		{
			// Jump to Clearing state
			State = PAINT_CLEAR;
		}
	}
}

//
// PAINT_CLEAR state function
//
void DrawClear(void)
{
	// Clear whole drawing area
	ili9341_clearAreaPixel(tftPaint, DRAW_WINDOW_X_START, DRAW_WINDOW_Y_START, DRAW_WINDOW_X_STOP, DRAW_WINDOW_Y_STOP - DRAW_WINDOW_Y_START, ILI9341_WHITE);
	// Redraw frame for the drawing area
	GFX_DrawRectangle(tftPaint, 0, 30, ILI9341_TFTWIDTH, ILI9341_TFTHEIGHT-60, ILI9341_BLACK);
	// Go back to Drawing state
	State = PAINT_DRAW;
}

void Paint(ili9342_t *tft, xpt2046_t *touch)
{
	switch(State)
	{
	case PAINT_INIT:
		InitScreen(tft, touch);
		break;
	case PAINT_DRAW:
		DrawScreen();
		  break;
	case PAINT_CLEAR:
		DrawClear();
		break;
	}
}
