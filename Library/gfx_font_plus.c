/*
 * gfx_font_plus.c
 *
 *  Created on: Nov 25, 2023
 *      Author: Jan Lukaszewicz
 */

//#include "main.h"
#include "gfx_font_plus.h"
#include "gfx_color.h"

#include "string.h"

const fontInfo_t *currentFont;

void FP_setFont(const fontInfo_t *font)
{
	currentFont = font;
}

uint8_t FP_getFontHeight(void)
{
	return currentFont->heightChar;
}

uint8_t FP_getFontStartChar(void)
{
	return currentFont->startChar;
}

uint8_t FP_getFontEndChar(void)
{
	return currentFont->endChar;
}

uint8_t FP_getFontSpaceWidth(void)
{
	return currentFont->spaceWidth;
}

char FP_decodePolishChar(uint8_t msbChar, uint8_t lsbChar)
{
	switch(msbChar) // Check first byte
	{
	case 0xC3: // Ó, ó
		switch(lsbChar) // Check second byte
		{
		case 0x93: // Ó
			return 0x84;
			break;
		case 0xB3: // ó
			return 0x8D;
			break;
		}
		break;
	case 0xC4: // Ą, Ę, Ć, ą, ę, ć
		switch(lsbChar) // Check second byte
		{
		case 0x84: // Ą
			return 0x7F;
			break;
		case 0x98: // Ę
			return 0x81;
			break;
		case 0x86: // Ć
			return 0x80;
			break;
		case 0x85: // ą
			return 0x88;
			break;
		case 0x99: // ę
			return 0x8A;
			break;
		case 0x87: // ć
			return 0x89;
			break;
		}
		break;
	case 0xC5: // Ł, Ń, Ś, Ź, Ż, ł, ń, ś, ź, ż
		switch(lsbChar) // Check second byte
		{
		case 0x81: // Ł
			return 0x82;
			break;
		case 0x83: // Ń
			return 0x83;
			break;
		case 0x9A: // Ś
			return 0x85;
			break;
		case 0xB9: // Ź
			return 0x86;
			break;
		case 0xBB: // Ż
			return 0x87;
			break;
		case 0x82: // ł
			return 0x8B;
			break;
		case 0x84: // ń
			return 0x8C;
			break;
		case 0x9B: // ś
			return 0x8E;
			break;
		case 0xBA: // ź
			return 0x8F;
			break;
		case 0xBC: // ż
			return 0x90;
			break;
		}
		break;
	}

	return 0;
}

uint8_t FP_putChar(ili9342_t *tft, char ch, uint16_t positionX, uint16_t positionY, uint16_t fontColor, BG_FONT backgroundTransparet, uint16_t backgroundColor)
{
	uint16_t pixelHeight;
	uint8_t selectByteInPixelWidth, charBitsLeft, charBits;
	uint8_t charNumber = ch - FP_getFontStartChar();

	if(ch == ' ')
	{
		if(backgroundTransparet == BG_COLOR)
		{
			GFX_DrawFillRectangle(tft, positionX, positionY, FP_SPACE_WIDTH * FP_getFontSpaceWidth(), currentFont->heightChar, backgroundColor);
		}

		return FP_SPACE_WIDTH * FP_getFontSpaceWidth();
	}

	if(charNumber > FP_getFontEndChar())
	{
		return 0;
	}

	const fontCharInfo_t *currentChar = &currentFont->descriptionChar[charNumber];

	uint8_t *charBitmapPtr = (uint8_t*)&currentFont->bitmapsChar[currentChar->offsetChar];

	for(pixelHeight = 0; pixelHeight < currentFont->heightChar; pixelHeight++)
	{
		charBitsLeft = currentChar->widthChar;

		for(selectByteInPixelWidth = 0; selectByteInPixelWidth <= (currentChar->widthChar - 1) / 8; selectByteInPixelWidth++) // select byte
		{
			uint8_t selectedByte = (uint8_t)*charBitmapPtr;

			if(charBitsLeft >= 8)
			{
				charBits = 8;
				charBitsLeft -= 8;
			}
			else
			{
				charBits = charBitsLeft;
			}

			for (uint8_t currentBit = 0; currentBit < charBits; currentBit++, selectedByte <<= 1) // select one bit from selected byte
			{
				if(selectedByte & 0x80)
				{
					GFX_DrawPixel(tft, positionX + (selectByteInPixelWidth * 8) + currentBit, positionY + pixelHeight, fontColor); // draw char pixel
				}
				else if(backgroundTransparet == BG_COLOR)
				{
					GFX_DrawPixel(tft, positionX + (selectByteInPixelWidth * 8) + currentBit, positionY + pixelHeight, backgroundColor); // draw background pixel
				}
			}

			charBitmapPtr++;
		}
	}

	return currentFont->descriptionChar[charNumber].widthChar;
}

uint8_t FP_putString(ili9342_t *tft, uint8_t *pStr, uint16_t positionX, uint16_t positionY, uint16_t fontColor, BG_FONT backgroundTransparet, uint16_t backgroundColor)
{
	uint8_t lenghtString = strlen((char*)pStr);
	uint8_t numberChar, charWidth;
	uint16_t shift = 0;
	uint8_t charToPrint;

	for (numberChar = 0; numberChar < lenghtString; numberChar++)
	{

		if((pStr[numberChar] <= 0xC5) && (pStr[numberChar] >= 0xC3))
		{
			charToPrint = FP_decodePolishChar(pStr[numberChar], pStr[numberChar +1]);
			numberChar++;
		}
		else
		{
			charToPrint = pStr[numberChar];
		}

		charWidth = FP_putChar(tft, charToPrint, positionX + shift, positionY, fontColor, backgroundTransparet, backgroundColor);

		shift = shift + charWidth;

		if(backgroundTransparet == BG_COLOR)
		{
			GFX_DrawFillRectangle(tft, positionX + shift, positionY, FP_getFontSpaceWidth(), FP_getFontHeight(), backgroundColor);
		}

		shift = shift + FP_getFontSpaceWidth();
	}


	return shift - FP_getFontSpaceWidth();
}











