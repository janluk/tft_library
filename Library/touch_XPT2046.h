/*
 * touch_XPT2046.h
 *
 *  Created on: Nov 30, 2023
 *      Author: Jan Lukaszewicz
 */

#ifndef TOUCH_XPT2046_H_
#define TOUCH_XPT2046_H_

#include "spi.h"
#include "stdint.h"
#include "tft_ili9341.h"

#define XPT2046_SAMPLE_INTERVAL 5

#define XTP2046_USE_CS 1
#define XTP2046_SPI_TIMEOUT 1000

#define XTP2046_MAX_TOUCH_SAMPLES 10

#define TOUCH_ROTATION	ILI9341_ROTATION
#define DISPLAY_HEIGHT	ILI9341_TFTHEIGHT
#define DISPLAY_WIDTH	ILI9341_TFTWIDTH



//
// Touch Screen States for State Machine
//
typedef enum
{
	XPT2046_IDLE,		// 0
	XPT2046_PRESAMPLING, // 1
	XPT2046_TOUCHED,	// 2
	XPT2046_RELEASED,	// 3
} xpt2046_state;


//
// Calibration data for make a new coordinates calculation
//
typedef struct
{
	long double alpha_x;
	long double beta_x;
	long double delta_x;
	long double alpha_y;
	long double beta_y;
	long double delta_y;
} calibData_t;

typedef struct
{
	SPI_HandleTypeDef *hspi;
	IRQn_Type IRQn;

	uint8_t sendBuffer[5];
	uint8_t channelSettingsX;
	uint8_t channelSettingsY;

	uint8_t receiveBuffer[5];

	uint8_t calibrationMode;
	calibData_t *calibrationData;
	volatile xpt2046_state stateTouch;

	uint16_t samplesTouch[2][XTP2046_MAX_TOUCH_SAMPLES]; // sample buffer for average coords calcualtion
	uint8_t sampleCounter; // how many samples we have now
	uint32_t sampleTimer; // software timer
}xpt2046_t;


void xpt2046_init(xpt2046_t *touch, SPI_HandleTypeDef *hspi, IRQn_Type touchIRQn);
void xpt2046_task(xpt2046_t *touch);
void xpt2046_IRQ(xpt2046_t *touch);

void xpt2046_getAverageTouchPoint(xpt2046_t *touch, uint16_t *x, uint16_t *y);
uint8_t xpt2046_isTouched(xpt2046_t *touch);

void xpt2046_calibrationTouch(xpt2046_t *touch, ili9342_t *tft);

#endif /* TOUCH_XPT2046_H_ */
