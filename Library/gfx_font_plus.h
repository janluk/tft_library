/*
 * gfx_font_plus.h
 *
 *  Created on: Nov 25, 2023
 *      Author: Jan Lukaszewicz
 */

#ifndef GFX_FONT_PLUS_H_
#define GFX_FONT_PLUS_H_

#include "tft_ili9341.h"
#include "stdint.h"

#define FP_SPACE_WIDTH 3

typedef enum
{
	BG_TRANSPARENT = 0,
	BG_COLOR = 1
}BG_FONT;

typedef struct
{
	uint8_t widthChar;
	uint16_t offsetChar;
} fontCharInfo_t;


typedef struct
{
	uint8_t heightChar;
	uint8_t startChar;
	uint8_t endChar;
	uint8_t spaceWidth;
	const fontCharInfo_t *descriptionChar;
	const uint8_t *bitmapsChar;
}fontInfo_t;

void FP_setFont(const fontInfo_t *font);

uint8_t FP_getFontHeight(void);
uint8_t FP_getFontStartChar(void);
uint8_t FP_getFontEndChar(void);
uint8_t FP_getFontSpaceWidth(void);

uint8_t FP_putString(ili9342_t *tft, uint8_t *pStr, uint16_t positionX, uint16_t positionY, uint16_t fontColor, BG_FONT backgroundTransparet, uint16_t backgroundColor);

#endif /* GFX_FONT_PLUS_H_ */
