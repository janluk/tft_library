/*
 * paint.h
 *
 *  Created on: Dec 16, 2023
 *      Author: Jan Lukaszewicz
 */

#ifndef PAINT_H_
#define PAINT_H_

#include "TFT_ILI9341.h"
#include "touch_XPT2046.h"

void Paint(ili9342_t *tft, xpt2046_t *touch);

#endif /* PAINT_H_ */
